;;;; $Id$
;;;; $Source$

;;;; See the LICENSE file for licensing information.

(in-package :cl-user)

(cl:eval-when (:execute :load-toplevel :compile-toplevel)
  (cl:defpackage :cl-syslog
      (:use :cl)
    (:nicknames :syslog)
    (:shadow :log)
    (:export :log :get-facility :get-priority
             :+log-pid+ :+log-cons+ :+log-odelay+
             :+log-ndelay+ :+log-nowait+ :+log-perror+)
    (:documentation "Common Lisp interface to syslog.")))
